﻿using System;
using Foundation;
using UIKit;
using testCore;

namespace test
{
	public partial class SampleViewCell : UITableViewCell
	{
		#region Fields
		private string _headerText;
		private string _contentText;

		public static readonly NSString Key = new NSString (SampleIconsConstants.CellID);
		public static readonly UINib Nib;
		#endregion

		#region Constructors
		static SampleViewCell ()
		{
			Nib = UINib.FromName (SampleIconsConstants.CellID, NSBundle.MainBundle);
		}
			
		public SampleViewCell (IntPtr handle) : base (handle)
		{
		}
		#endregion

		#region Properties
		public UIImageView CellImage
		{
			get
			{
				return Thumbnail;
			}
			set
			{
				this.Thumbnail = value;
			}
		}

		public string CellHeader
		{
			get
			{ 
				return _headerText;
			}
			set
			{
				_headerText = value;
				Header.Text = _headerText;
			}
		}

		public string CellContent
		{
			get
			{ 
				return _contentText;
			}
			set
			{
				_contentText = value;
				Content.Text = _contentText;
			}
		}
		#endregion

		#region Public methods
		public static SampleViewCell CellCreate()
		{
			return (SampleViewCell) Nib.Instantiate (null, null)[0];
		}
		#endregion
	}
}
