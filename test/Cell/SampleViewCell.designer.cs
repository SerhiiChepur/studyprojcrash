// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using testCore;
using UIKit;

namespace test
{
    [Register ("SampleViewCell")]
    partial class SampleViewCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Content { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Header { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView Thumbnail { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (Content != null) {
                Content.Dispose ();
                Content = null;
            }

            if (Header != null) {
                Header.Dispose ();
                Header = null;
            }

            if (Thumbnail != null) {
                Thumbnail.Dispose ();
                Thumbnail = null;
            }
        }
    }
}