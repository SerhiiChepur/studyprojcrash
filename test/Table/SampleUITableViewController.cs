using Foundation;
using System;
using UIKit;
using testCore;
using System.Collections.Generic;
using testDl.Interfaces;
using testCore.Enums;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Linq;

namespace test
{
	public partial class SampleUITableViewController : UITableViewController
	{
		public SampleUITableViewController (IntPtr handle) : base (handle)
		{						
			Application.DataContext.DataLoading += OnDataLoadingStatus;
			Application.DataContext.DataChanged += OnDataChanged;
		}

		#region Properties
		SampleTableSource DataSource{ get; set; }
		#endregion

		#region Public methods
		public async override void ViewDidLoad ()
		{			
			base.ViewDidLoad ();
			var data = await Application.DataContext.GetDataAsync ();
			var views = GetViews (data);
			DataSource = new SampleTableSource (this, ItemsTable, views);
			ItemsTable.Source = DataSource;
			ItemsTable.ReloadData ();
			var indexes = ItemsTable.IndexPathsForVisibleRows;
			await UpdateThumbnails (indexes.Select (s => s.Row));
			ItemsTable.BeginUpdates ();
			ItemsTable.ReloadRows (indexes, UITableViewRowAnimation.Automatic);
			ItemsTable.EndUpdates ();
		}

		public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
		{
			if (segue.Identifier == SampleIconsConstants.CellSelectedSegueId)
			{
				var indexPath = ItemsTable.IndexPathForSelectedRow;
				var item = DataSource.Data[indexPath.Row];
				var detailController = ((UINavigationController)segue.DestinationViewController).TopViewController as DetailViewCustom;
				if (detailController != null) detailController.Model = item;
			}
		}

		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
		{
			return true;
		}
		#endregion

		#region Private methods

		private async Task UpdateThumbnails(IEnumerable<int> indexes)
		{
			if (indexes.Max () < DataSource.Data.Count()) 
			{
				var guidList = new List<string>();
				foreach(var index in indexes)
				{
					guidList.Add(DataSource.Data [index].Id);
				}
				var updated = await Application.DataContext.UpdateData (guidList, WImgUpdateMode.Icon);
				if (!updated.Any ())
					return;
				var _preparedViews = GetViews (updated);
				var d = indexes.ToList ();
				foreach (var index in d) 
				{
					var itemId = DataSource.Data [index].Id;
					if (guidList.Contains(itemId )) 
					{
						DataSource.Data [index] = _preparedViews.First (firstStatement => firstStatement.Id == itemId);
					}
				}
			}
		}

		private void OnDataLoadingStatus (object sender, StatusArgs e)
		{			
			switch (e.Status) 
			{
			case LoadingStatus.LoadError:
				{					
					UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
					break;
				}
			case LoadingStatus.LoadSuccess:
				{
					UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
					break;
				}
			case LoadingStatus.StartLoad:
				{
					UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;
					break;
				}
			}
		}

		private void OnDataChanged(object sender, UpdatedDataEventArgs e)
		{
			if (e.RecordIds.Any())
			{
				var indexes = new List<int>(e.RecordIds.Count());
				foreach (var item in DataSource.Data.Where(whereStatesment => e.RecordIds.Contains(whereStatesment.Id)))
				{
					indexes.Add(DataSource.Data.IndexOf(item));
				}
				Parallel.ForEach(indexes, indexItem =>
				{
					var id = DataSource.Data[indexItem].Id;
					//DataSource.Data[indexItem] =
					var convertedView = GetViews(Application.DataContext.GetData(id));
					if (convertedView.Any()) DataSource.Data[indexItem] = convertedView.First();
				}
				                );
			}
		}

		private async Task<IIosIconViewModel> UpdateImages(IIosIconViewModel iosIconViewModel)
		{
			var updated = await Application.DataContext.UpdateData(iosIconViewModel.Id, WImgUpdateMode.BigImg);
			if (updated.Any())
			{
				return GetViews(new List<IIconDataModel>() { updated.First() }).First();
			}
			return null;
		}

		private IEnumerable<IIosIconViewModel> GetViews(IEnumerable<IIconDataModel> data)
		{
			var result = new ConcurrentBag<IIosIconViewModel> ();
			foreach(var dataItem in data)
				{
					result.Add(new IosIconViewModel(dataItem) );
				}
			return result;
		}
		#endregion
	}
}
