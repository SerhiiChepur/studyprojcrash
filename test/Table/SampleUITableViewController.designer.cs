// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace test
{
	[Register ("SampleUITableViewController")]
	partial class SampleUITableViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITableView ItemsTable { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (ItemsTable != null) {
				ItemsTable.Dispose ();
				ItemsTable = null;
			}
		}
	}
}
