﻿using System;
using UIKit;
using Foundation;
using testDl.Interfaces;

namespace test
{
	public class IosIconViewModel : IIosIconViewModel
	{
		public IosIconViewModel (IIconDataModel dataModel)
		{
			Id = dataModel.Id;
			Name = dataModel.Name;
			Description = dataModel.Description;
			Thumbnail = UIImage.LoadFromData (NSData.FromArray(dataModel.Thumbnail));
			Image = UIImage.LoadFromData(NSData.FromArray(dataModel.Image));
		}

		#region IIosIconViewModel implementation

		public string Id { get; set;}

		public string Name { get; set;}

		public string Description { get; set;}

		public UIImage Thumbnail { get; set;}

		public UIImage Image { get; set;}

		#endregion
	}
}

