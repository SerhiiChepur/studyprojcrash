using System;
using UIKit;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using testDl.Interfaces;
using System.Linq;
using testCore.Enums;

namespace test
{
	public partial class DetailViewCustom : UIViewController
	{
		public IIosIconViewModel Model{ get; set;}

		public DetailViewCustom (IntPtr handle) : base (handle)
		{			
		}

		public async override void ViewDidLoad ()
		{			
			base.ViewDidLoad ();

			if (Model != null) 
			{				
				ImageForDisplay.Image = Model.Image;
				if (Application.DataContext.NeedToUpdate(Model.Id))
				{
					var res = await UpdateImages(Model);
					lock(this)
					{
						if (res != null) Model = res;
						ImageForDisplay.Image = Model.Image;
					}
				}
			}
		}

		private async Task<IIosIconViewModel> UpdateImages(IIosIconViewModel iosIconViewModel)
		{
			var updated = await Application.DataContext.UpdateData(iosIconViewModel.Id, WImgUpdateMode.BigImg);
			if (updated.Any())
			{
				return GetViews(new List<IIconDataModel>() { updated.First() }).First();
			}
			return null;
		}

		private IEnumerable<IIosIconViewModel> GetViews(IEnumerable<IIconDataModel> data)
		{
			var result = new ConcurrentBag<IIosIconViewModel>();
			foreach (var dataItem in data)
			{
				result.Add(new IosIconViewModel(dataItem));
			}
			return result;
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			if (!UIDevice.CurrentDevice.Name.Contains("Phone"))
			{
				AddButtonForPortrait();
			}
		}

		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
		{
			return true;
		}

		public void AddButtonForPortrait()
		{
			NavigationItem.LeftBarButtonItem = SplitViewController.DisplayModeButtonItem;
			NavigationItem.LeftBarButtonItem.Title = "Menu";
			NavigationItem.LeftItemsSupplementBackButton = true;
		}

		public void RemoveButtonForPortrait()
		{
			NavigationItem.LeftBarButtonItem = null;
		}
	}
}
