﻿using System;
using UIKit;

namespace test
{
	public interface IIosIconViewModel
	{
		string Id{ get; set;}
		string Name {get;set;}
		string Description {get;set;}
		UIImage Thumbnail {get; set;}
		UIImage Image {get; set;}
	}
}

