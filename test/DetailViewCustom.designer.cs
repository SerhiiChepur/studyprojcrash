// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace test
{
    [Register ("DetailViewCustom")]
    partial class DetailViewCustom
    {
        [Outlet]
        UIKit.UIImageView ImageForDisplay { get; set; }


        [Outlet]
        UIKit.UINavigationBar NBar { get; set; }


        [Outlet]
        UIKit.UILabel TestLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ImageForDisplay != null) {
                ImageForDisplay.Dispose ();
                ImageForDisplay = null;
            }
        }
    }
}