﻿using System;
using UIKit;

namespace test
{
	public class CustomSplitViewDelegate : UISplitViewControllerDelegate
	{
		#region Private properties
		private bool IsMobile { get; set; }
		#endregion

		public CustomSplitViewDelegate()
		{
			IsMobile = UIDevice.CurrentDevice.Name.Contains("Phone");
		}

		public override bool ShouldHideViewController(UISplitViewController svc, UIViewController viewController, UIInterfaceOrientation inOrientation)
		{
				return inOrientation == UIInterfaceOrientation.Portrait || inOrientation == UIInterfaceOrientation.PortraitUpsideDown;
		}

		public override void WillHideViewController(UISplitViewController svc, UIViewController aViewController, UIBarButtonItem barButtonItem, UIPopoverController pc)
		{
			if (!IsMobile)
			{
				var detailView = ((UINavigationController)svc.ViewControllers[1]).TopViewController as DetailViewCustom;
				detailView.AddButtonForPortrait();
			}
		}

		public override void WillShowViewController(UISplitViewController svc, UIViewController aViewController, UIBarButtonItem button)
		{
			if (!IsMobile)
			{
				var detailView = ((UINavigationController)svc.ViewControllers[1]).TopViewController as DetailViewCustom;
				detailView.RemoveButtonForPortrait();
			}
		}
	}
}

