using System;
using UIKit;

namespace test
{
	public partial class RootSplitController : UISplitViewController
	{		
		public RootSplitController (IntPtr handle) : base (handle)
		{
			if (UIDevice.CurrentDevice.Name.Contains("Phone"))
			{
				PreferredDisplayMode = UISplitViewControllerDisplayMode.PrimaryOverlay;
				ShouldHideViewController += HideShowMainMenu;
			}
			else 
			{
				Delegate = new CustomSplitViewDelegate();
			}
		}

		public bool HideShowMainMenu (UISplitViewController svc, UIViewController viewController, UIInterfaceOrientation inOrientation)
		{			
			switch (inOrientation)
			{
			case UIInterfaceOrientation.Portrait:
				{
					return false;
				}
			default: return true;
			}
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
		}
	}
}
