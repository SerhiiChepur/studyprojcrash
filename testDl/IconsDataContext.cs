﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using testDl.Interfaces;
using testDl.Models;
using testCore;
using testCore.Enums;
using testCore.Interfaces;
using Realms;

namespace testDl
{
	public class IconsDataContext : IIconsDataContext
	{
		#region Fields
		private IIconsDataProvider _dataProvider;
		private byte[] _defaultImage;
		#endregion

		#region Constructors
		public IconsDataContext (IIconsDataProvider src, byte[] defaultImage)
		{
			VerifyDataProvider(src);
			VerifyDefImg(defaultImage);
			_dataProvider = src;
			_defaultImage = defaultImage;
			_dataProvider.DataLoading += OnProviderDataLoading;
		}
		#endregion

		#region Events
		public event EventHandler<UpdatedDataEventArgs> DataChanged;
		public event EventHandler<StatusArgs> DataLoading;
		#endregion

		#region Properties
		public IIconsDataProvider DataProvider
		{
			get
			{
				return _dataProvider;
			}
			set
			{
				VerifyDataProvider(value);
				_dataProvider = value;
				_dataProvider.DataLoading += OnProviderDataLoading;
			}
		}

		public byte[] DefaultImage
		{
			get 
			{
				return _defaultImage;
			}
			set
			{
				VerifyDefImg(value);
				_defaultImage = value;
			}
		}
		#endregion

		#region Public methods
		public async Task<IEnumerable<IIconDataModel>> UpdateData(IEnumerable<string> data, WImgUpdateMode mode = WImgUpdateMode.Icon)
		{			
			switch (mode) {
			//case WImgUpdateMode.All: {break;}
			//case WImgUpdateMode.Text: {break;}
			case WImgUpdateMode.Icon:
				{
					var result = await ImgUpdate (data, WImgUpdateMode.Icon);
						return result;
				}
			case WImgUpdateMode.BigImg: 
				{
					var result = await ImgUpdate (data, WImgUpdateMode.BigImg);
					var args = new UpdatedDataEventArgs() { RecordIds = result.Select(s => s.Id) };
					OnDataChanged(this, args);
					return result;
				}
			default:
				{
					throw new NotImplementedException (string.Format ("{0} update mode is not implemented", mode.ToString()));
				}
			}
		}

		public IEnumerable<IIconDataModel> GetData(IEnumerable<string> ids)
		{
			if (ids == null || !ids.Any()) throw new ArgumentException(SampleIconsConstants.MsgErrEmptyRecordLst);
			var result = new List<IIconDataModel>(ids.Count());
			foreach (var id in ids)
			{
				result.Add(GetData(id).First());
			}
			return result;
		}

		public async Task<IEnumerable<IIconDataModel>> GetDataAsync()
		{
			var unpreparedData = await _dataProvider.GetDataAsync ();
			AddToDb (unpreparedData);
			var dbRecords = await ModelConvertAsync ();
			return dbRecords;
		}

		public bool NeedToUpdate(string id)
		{
			var realm = Realm.GetInstance();
			var all = realm.All<IconDataModel>().ToList();
			if (all.Any(anyStatement => anyStatement.Id == id))
			{
				var d = all.First(anyStatement => anyStatement.Id == id);
				return d.Image.ImageName == SampleIconsConstants.DefaultImgKeyName;
			}
			return false;
		}

		public bool NeedToUpdate(Guid id)
		{
			return NeedToUpdate(id.ToString(SampleIconsConstants.GuidFormatString));
		}

		public async Task<IEnumerable<IIconDataModel>> UpdateData(string data, WImgUpdateMode mode = WImgUpdateMode.Icon)
		{
			return await UpdateData(new List<string>() { data }, WImgUpdateMode.BigImg);
		}

		public IEnumerable<IIconDataModel> GetData(string id)
		{
			return ModelConvert(new List<string>{id});
		}

		private IEnumerable<IIconDataModel> ModelConvert(IEnumerable<string> iDs)
		{
			var __realm = Realm.GetInstance();
			IEnumerable<IIconDataObject> data =
						(iDs == null)
						? __realm.All<IconDataModel>().ToList()
						: __realm.All<IconDataModel>().ToList()
							.Where(whereStatement => iDs.Contains(whereStatement.Id));
			var srcData = new List<IIconDataObject>(data);
			var result = new List<IIconDataModel>();
			if (srcData != null)
			{
				foreach (var selectedItem in srcData)
				{
					result.Add(new IconViewModel()
					{
						Id = selectedItem.Id,
						Name = selectedItem.Name,
						Description = selectedItem.Description,
						Thumbnail = Convert.FromBase64String(selectedItem.Thumbnail.ImageBytes),
						Image = Convert.FromBase64String(selectedItem.Image.ImageBytes)
					});
				}
			}
			__realm.Close();
			return result;
		}

	#endregion

		#region Private methods
		private void VerifyDataProvider(IIconsDataProvider provider)
		{
			if (provider == null) throw new ArgumentNullException(SampleIconsConstants.MsgErrProviderNull);
		}

		private void VerifyDefImg(byte[] imgBytes)
		{
			if (imgBytes == null || imgBytes.Length == 0) throw new ArgumentNullException(SampleIconsConstants.MsgErrIncorrectDefImg);
		}

		private void AddToDb(IEnumerable<IIconsDataProviderItem> unpreparedData)
		{
			try{	
				Realm.DeleteRealm(RealmConfiguration.DefaultConfiguration);
				var realm = Realm.GetInstance ();
				var transaction = realm.BeginWrite ();
				var defImg = TakeDefaultImg (realm);
					transaction.Commit ();
				foreach (var item in unpreparedData) 
				{		
					using (transaction = realm.BeginWrite ())
					{
						var presentImages = realm.All<IconImageModel> ();
						var dataObject = realm.CreateObject<IconDataModel> ();
						dataObject.Id = Guid.NewGuid().ToString(SampleIconsConstants.GuidFormatString);
						dataObject.Name = item.Name;
						dataObject.Description = item.Description;
						dataObject.ImageName = item.Image;
						dataObject.ThumbnailName = item.Thumbnail;
						//Init imgs by default picture or present img data
						var v1 =item.Image;
						var t1 = presentImages.Where (w => w.ImageName == v1).Any();
						if(t1)
						{
							var t11 = presentImages.Where (w => w.ImageName == v1).First();
							dataObject.Image = t11;
						} else{
							dataObject.Image = defImg;
						}


						var v2 =item.Thumbnail;
						var t2 = presentImages.Where (w => w.ImageName == v2).Any();
						if(t2)
						{
							var t22 = presentImages.Where (w => w.ImageName == v2).First();
							dataObject.Thumbnail = t22;
						} else{
							dataObject.Thumbnail = defImg;
						}
						transaction.Commit ();
					}

				}
			}
			catch(Exception e) 
			{
				// Should be logged
				var d = e;
			}
		}

		private async Task<IEnumerable<IIconDataModel>> ImgUpdate (IEnumerable<string> data, WImgUpdateMode mode)
		{			
			var _realm = Realm.GetInstance ();
			var _tmp = _realm.All<IconDataModel> ().ToList().Where (w => data.Contains (w.Id));
			var IsIcon = mode == WImgUpdateMode.Icon;
			var imgsNames = (IsIcon)
				? _tmp.Select (s => s.ThumbnailName).Distinct ()
				: _tmp.Select (s => s.ImageName).Distinct ();

			var takenImgesValues = await _dataProvider.GetImageByName (imgsNames);
			foreach (var imgItem in takenImgesValues) 
			{
				var tmpName = imgItem.Key;
				using (var transactionObj = _realm.BeginWrite ()) 
				{
					var presentImgs = _realm.All<IconImageModel> ().Where (w => w.ImageName == tmpName);
					if (!presentImgs.Any ()) 
					{
						var newRecord = _realm.CreateObject<IconImageModel> ();
						newRecord.ImageName = tmpName;
						newRecord.ImageBytes = Convert.ToBase64String (imgItem.Value);
						transactionObj.Commit ();
					}
					else 
					{
						var presentRecord = presentImgs.First ();
						presentRecord.ImageBytes = Convert.ToBase64String (imgItem.Value);
						transactionObj.Commit ();
					}
				}
				var preparedImgModel = _realm.All<IconImageModel> ().Where (preparedImgStatement => preparedImgStatement.ImageName == tmpName).First ();
				var unitializedDataModels = (IsIcon) 
					? _realm.All<IconDataModel> ().ToList().Where (w => w.ThumbnailName == tmpName & w.Thumbnail.ImageName == SampleIconsConstants.DefaultImgKeyName)
					: _realm.All<IconDataModel> ().ToList().Where (w => w.ImageName == tmpName & w.Image.ImageName == SampleIconsConstants.DefaultImgKeyName);
				if (unitializedDataModels.Any ()) 
				{
					using (var finalTransaction = _realm.BeginWrite ()) 
					{
						var idsToUpdate = unitializedDataModels.Select (s => s.Id);
						foreach (var id in idsToUpdate) 
						{
							var tmpId = id;
							switch (IsIcon) 
							{
							case true:
								{
									_realm.All<IconDataModel> ().Where (idStatement => idStatement.Id == tmpId).First ().Thumbnail = preparedImgModel;
									finalTransaction.Commit ();
									break;
								}
							case false:
								{
									_realm.All<IconDataModel> ().Where (idStatement => idStatement.Id == tmpId).First ().Image = preparedImgModel;
									finalTransaction.Commit ();
									break;
								}
							}
						}
					}
				}
			}
			return await ModelConvertAsync (data);
		}

		private IconImageModel TakeDefaultImg(Realm realm)
		{
			var imgs = realm.All<IconImageModel> ();
			if (imgs.Where (w => w.ImageName == SampleIconsConstants.DefaultImgKeyName).Any()) 
			{
				return imgs.Where (w => w.ImageName == SampleIconsConstants.DefaultImgKeyName).First ();
			}
			var defaultImg = realm.CreateObject<IconImageModel> ();
			defaultImg.ImageName = SampleIconsConstants.DefaultImgKeyName;
			defaultImg.ImageBytes = Convert.ToBase64String (_defaultImage);
			return defaultImg;
		}

		private async Task<IEnumerable<IIconDataModel>> ModelConvertAsync(IEnumerable<string> iDs = null)
		{					
			var converted = await Task.Run(()=>
			{				
				return ModelConvert(iDs);
			});

			return converted;
		}
				
		private void OnProviderDataLoading (object sender, StatusArgs e)
		{
			if (DataLoading != null) 
			{
				DataLoading (this, e);
			}
		}

		private void OnDataChanged(object sender, UpdatedDataEventArgs e)
		{
			if (DataChanged != null)
			{
				DataChanged(this, e);
			}
		}
		#endregion

		#region Unimlemented
		public IEnumerable<IIconDataModel> GetData()
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}

