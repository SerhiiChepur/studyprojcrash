﻿using System;
using Realms;
using testDl.Models;

namespace testDl.Interfaces
{
	public interface IIconDataObject
	{
		string Id{ get; set;}
		string Name {get; set;}
		string Description {get; set;}
		//Images byte arrays converted to string
		string ThumbnailName {get; set;}
		string ImageName {get; set;}
		IconImageModel Thumbnail{ get; set;}
		IconImageModel Image{ get; set;}
	}
}

