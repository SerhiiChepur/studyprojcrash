﻿using System;

namespace testDl.Interfaces
{
	public interface ISampleIconAppImage
	{
		string ImageName { get; set; }
		string ImageBytes{ get; set; }
	}
}

