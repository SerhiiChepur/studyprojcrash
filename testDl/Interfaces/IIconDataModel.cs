﻿using System;

namespace testDl.Interfaces
{
	public interface IIconDataModel
	{
		string Id{ get; set;}
		string Name {get; set;}
		string Description {get; set;}
		byte[] Thumbnail {get; set;}
		byte[] Image {get; set;}
	}
}

