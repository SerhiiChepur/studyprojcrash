﻿using System;
using Realms;
using testDl.Interfaces;

namespace testDl.Models
{
	public class IconImageModel : RealmObject, ISampleIconAppImage
	{
		public IconImageModel ()
		{
		}

		#region ISampleIconAppImage implementation

		[ObjectId]
		[Indexed]
		public string ImageName { get; set;}

		public string ImageBytes { get; set; }


		#endregion
	}
}

