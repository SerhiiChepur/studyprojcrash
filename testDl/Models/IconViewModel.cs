﻿using System;
using testDl.Interfaces;

namespace testDl.Models
{
	public class IconViewModel : IIconDataModel
	{
		#region Properties

		public string Id {get;set;}

		public string Name {get;set;}

		public string Description {get;set;}

		public byte[] Thumbnail {get; set;}

		public byte[] Image {get; set;}

		#endregion
	}
}

