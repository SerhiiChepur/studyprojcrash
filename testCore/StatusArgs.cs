﻿using System;
using testCore.Enums;

namespace testCore
{
	public class StatusArgs : EventArgs
	{
		public StatusArgs (LoadingStatus status)
		{
			Status = status;
		}

		public LoadingStatus Status { get; set; }
	}
}

