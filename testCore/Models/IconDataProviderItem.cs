﻿using System;
using testCore.Interfaces;

namespace testCore.Models
{
	public class IconDataProviderItem : IIconsDataProviderItem
	{
		#region IIconsDataProviderItem implementation

		public string Thumbnail { get; set;}

		public string Image { get; set;}

		public string Name { get; set;}

		public string Description { get; set;}

		#endregion
	}
}

