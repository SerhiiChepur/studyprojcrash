﻿using System;

namespace testCore.Enums
{
	[Flags]
	public enum WImgUpdateMode
	{
		All 	= 0,
		Text 	= 1,
		Icon 	= 2,
		BigImg 	= 4
	}
}

