﻿using System;

namespace testCore.Enums
{
	public enum LoadingStatus
	{
		LoadError,
		LoadSuccess,
		StartLoad
	}
}

