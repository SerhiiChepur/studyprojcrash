﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using testCore;

namespace testCore.Interfaces
{
	public interface IIconsDataProvider
	{		
		event EventHandler<StatusArgs> DataLoading;
		IEnumerable<IIconsDataProviderItem> GetData ();
		Task<IEnumerable<IIconsDataProviderItem>> GetDataAsync();
		Task<byte[]> GetImageByName (string imgName,bool retryIfBuffTooSmall);
		Task<IDictionary<string, byte[]>> GetImageByName (IEnumerable<string> imgName);	
	}
}

