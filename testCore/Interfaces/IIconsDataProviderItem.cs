﻿using System;
using Newtonsoft.Json;

namespace testCore.Interfaces
{
	public interface IIconsDataProviderItem
	{
		[JsonProperty("thumbnail")]
		string Thumbnail {get; set;}
		[JsonProperty("image")]
		string Image {get; set;}
		[JsonProperty("name")]
		string Name {get; set;}
		[JsonProperty("description")]
		string Description {get; set;}
	}
}

